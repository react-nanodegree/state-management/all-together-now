import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

/*
This exercise will help you practice many of your newly aquired React skills.

The instructions are included in the `instructions.md` file.
*/

class SendMessageForm extends Component {
  constructor(props){
    super(props)
    this.sendMessage = this.sendMessage.bind(this)
    this.onMessageFormChange = this.onMessageFormChange.bind(this)
  }
  onMessageFormChange(event){
    this.props.onMessageFormChange(event, this.props.chatData)
  }
  sendMessage(event){
    this.props.sendMessageHandler(event, this.props.chatData)
  }
  render() {
    return (
      <form className="input-group" action='#' onSubmit={this.sendMessage}>
        <input
          type="text"
          className="form-control"
          placeholder="Enter your message..."
          onChange={this.onMessageFormChange}
          onFocus={this.onMessageFormChange}
          value={this.props.chatData.text}
        />
        <div className="input-group-append">
          <button className="btn submit-button" disabled={this.props.disabled}>
            SEND
          </button>
        </div>
      </form>
    )
  }
}

class ChatWindow extends Component {
  isDisabled() {
    return this.props.chatData.disabled
  }
  render() {
    return (
      <div className="chat-window">
        <h2>Super Awesome Chat</h2>
        <div className="name sender">{this.props.chatData.user}</div>

        <ul className="message-list">
          {this.props.messages.map((message, index) => (
            <li
              key={index}
              className={
                message.from === this.props.chatData.user ? 'message sender' : 'message recipient'
              }
            >
              <p>{`${message.from}: ${message.text}`}</p>
            </li>
          ))}
        </ul>

        <div>
          <SendMessageForm
            disabled={this.isDisabled()}
            sendMessageHandler={this.props.sendMessage}
            chatData={this.props.chatData}
            onMessageFormChange={this.props.onMessageFormChange}
          />
        </div>
      </div>
    )
  }
}

class App extends Component {
  constructor(props){
    super(props)
    this.sendMessage = this.sendMessage.bind(this)
    this.onMessageFormChange = this.onMessageFormChange.bind(this)
  }
  state = {
      chatData : [
        {
          chatWindowId: 0,
          conversationId: 0,
          user: 'Amy',
          disabled: false,
          text: ''
        },
        {
          chatWindowId: 1,
          conversationId: 0,
          user: 'Jon',
          disabled: false,
          text: ''
        }
      ],
      /*
      [{ to: 'Jon', from: 'Amy', 'text': 'Hi, Jon!', conversationId: 0}]
      */
      messages : []
  }

  sendMessage(event, localChatData){
    event.preventDefault()
    event.persist()

    if(localChatData.disabled ||
        localChatData.text === '' ||
        localChatData.text === undefined){
      return
    }

    let recipientChatData = []
    this.setState((currentState) => {
      recipientChatData = currentState.chatData.filter((chatData) => (
        chatData.user !== localChatData.user
      ))

      recipientChatData.forEach((chatData) => {
        currentState.messages.push({
          to: chatData.user,
          from: localChatData.user,
          text: localChatData.text,
          conversationId: chatData.conversationId
        })
      })

      let chataDataIndex = currentState.chatData.findIndex((chatData) => {
         return chatData.user === localChatData.user
      })
      currentState.chatData[chataDataIndex].text = ''

      return currentState;
    })
  }

  onMessageFormChange(event, localChatData){
    localChatData.text = event.target.value

    let disabled = false;

    if(localChatData.text === '' || localChatData.text === undefined ){
      disabled = true;
    }

    this.setState((currentState) => {
      let chataDataIndex = currentState.chatData.findIndex((chatData) => {
         return chatData.user === localChatData.user
      })

      currentState.chatData[chataDataIndex].disabled = disabled
      currentState.chatData[chataDataIndex].text = localChatData.text
      return {chatData: currentState.chatData}
    })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">ReactND - Coding Practice</h1>
        </header>
        <div className="container">
          {
            this.state.chatData.map((chatData) => (
              <ChatWindow
                key={chatData.chatWindowId}
                chatData={chatData}
                messages={this.state.messages}
                sendMessage={this.sendMessage}
                onMessageFormChange={this.onMessageFormChange}
              />
            ))
          }
        </div>
      </div>
    );
  }
}

export default App;
